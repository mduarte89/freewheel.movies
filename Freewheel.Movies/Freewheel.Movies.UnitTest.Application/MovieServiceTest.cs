﻿namespace Freewheel.Movies.UnitTest.Application
{
    using Freewheel.Movies.Application.Services;
    using Freewheel.Movies.Data;
    using Freewheel.Movies.Domain.Entities;
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using Freewheel.Movies.UnitTest.Application.Helpers;
    using Moq;
    using Moq.Language;
    using Moq.Language.Flow;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;

    /// <summary>
    /// Movie service test
    /// </summary>
    [TestFixture]
    public class MovieServiceTest
    {
        /// <summary>
        /// Test if no movies are available for a year of release it should return empty list
        /// </summary>
        [Test]
        public void MovieService_NoMoviesAvailableForYearOrRelease_ShouldReturnEmptyList()
        {
            // Arrange
            #region arrange
            Genre genre = new Genre()
            {
                Id = 1,
                Name = "comedy"
            };

            Rating rating = new Rating()
            {
                Value = 1
            };

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                    Genres = new List<Genre>()
                    {
                        genre
                    },
                    Ratings = new List<Rating>()
                    {
                       rating
                    }
                },
            }.AsQueryable();


            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);
            MovieSearchDetailsViewModel emptyMovieSearchDetailsViewModel = new MovieSearchDetailsViewModel() { YearOfRelease = 1999 };

            // Act
            IMovieService movieService = new MovieService(dbContextFunc, formatter.Object);
            var movies = movieService.GetAsync(emptyMovieSearchDetailsViewModel).Result;

            // Assert
            Assert.AreEqual(0, movies.Count());
        }

        /// <summary>
        /// Test if a movie with a title (containing) exists it should return one movie
        /// </summary>
        [Test]
        public void MovieService_OneMoviesAvailableWithTitle_ShouldReturnOneMovie()
        {
            // Arrange
            #region arrange
            Genre genre = new Genre()
            {
                Id = 1,
                Name = "comedy"
            };

            Rating rating = new Rating()
            {
                Value = 1
            };

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                    Genres = new List<Genre>()
                    {
                        genre
                    },
                    Ratings = new List<Rating>()
                    {
                       rating
                    }
                },
            }.AsQueryable();

            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);

            MovieSearchDetailsViewModel emptyMovieSearchDetailsViewModel = new MovieSearchDetailsViewModel() { Title = "incep" };

            // Act
            IMovieService movieService = new MovieService(dbContextFunc, formatter.Object);
            var movies = movieService.GetAsync(emptyMovieSearchDetailsViewModel).Result;

            // Assert
            Assert.AreEqual(1, movies.Count());
        }
    }
}
