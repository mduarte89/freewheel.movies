﻿namespace Freewheel.Movies.UnitTest.Application
{
    using Freewheel.Movies.Application.Exceptions;
    using Freewheel.Movies.Application.Services;
    using Freewheel.Movies.Domain.Entities;
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using Freewheel.Movies.UnitTest.Application.Helpers;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// User ratings service test
    /// </summary>
    [TestFixture]
    public class UserRatingsServiceTest
    {
        /// <summary>
        /// If the user doesnt have any ratings it should return an empty list of movies
        /// </summary>
        [Test]
        public void UserRating_UserDoesntHaveAnyRatings_ShouldReturnEmptyList()
        {
            // Arrange
            #region arrange
            Genre genre = new Genre()
            {
                Id = 1,
                Name = "comedy"
            };

            User user = new User()
            {
                Id = 1
            };

            Rating rating = new Rating()
            {
                Value = 1,
                User = user,
                UserId = 1
            };

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                    Genres = new List<Genre>()
                    {
                        genre
                    },
                    Ratings = new List<Rating>()
                    {
                       rating
                    }
                },
            }.AsQueryable();

            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);

            // Act
            IUserRatingsService userRatingService = new UserRatingsService(dbContextFunc, formatter.Object);
            var movies = userRatingService.GetAsync(2).Result;

            // Assert
            Assert.AreEqual(0, movies.Count());
        }

        /// <summary>
        /// The user has any ratings it should return one movie
        /// </summary>
        [Test]
        public void UserRating_UserHasOneMovieWithRatings_ShouldReturnOneMovie()
        {
            // Arrange
            #region arrange
            Genre genre = new Genre()
            {
                Id = 1,
                Name = "comedy"
            };

            User user = new User()
            {
                Id = 1
            };

            Rating rating = new Rating()
            {
                Value = 1,
                User = user,
                UserId = 1
            };

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                    Genres = new List<Genre>()
                    {
                        genre
                    },
                    Ratings = new List<Rating>()
                    {
                       rating
                    }
                },
            }.AsQueryable();

            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);

            // Act
            IUserRatingsService userRatingService = new UserRatingsService(dbContextFunc, formatter.Object);
            var movies = userRatingService.GetAsync(1).Result;

            // Assert
            Assert.AreEqual(1, movies.Count());
        }

        /// <summary>
        /// The user does not exist should return user not found exception
        /// </summary>
        [Test]
        public void MovieService_AddingRateToUserThatDoesNotExist_ShouldReturnUserNotFoundException()
        {
            // Arrange
            #region arrange

            var users = new List<User>()
            {
                new User()
                {
                    Id = 1
                }
                
            }.AsQueryable();

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                },
            }.AsQueryable();

            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);
            var fakeUsersAsynEnumerable = new FakeDbAsyncEnumerable<User>(users);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var mockDbSetUsers = new Mock<IDbSet<User>>();
            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().Provider);

            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().Expression);

            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().ElementType);

            mockDbSetUsers.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<User>)fakeUsersAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);
            dbContext.Setup(x => x.Users).Returns(mockDbSetUsers.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);

            // Act
            IUserRatingsService userRatingService = new UserRatingsService(dbContextFunc, formatter.Object);

            // Assert
            Assert.ThrowsAsync<UserNotFoundException>(async () => await userRatingService.Add(2, 2, 2));
        }

        /// <summary>
        /// The movie does not exist should return movie not found exception
        /// </summary>
        [Test]
        public void MovieService_AddingRateToMovieThatDoesNotExist_ShouldReturnMovieNotFoundException()
        {
            // Arrange
            #region arrange

            var users = new List<User>()
            {
                new User()
                {
                    Id = 1
                }

            }.AsQueryable();

            var fakeMovies = new List<Movie>()
            {
                new Movie()
                {
                    Id =1,
                    Title = "Inception",
                    YearOfRelease = 2000,
                },
            }.AsQueryable();

            var fakeAsynEnumerable = new FakeDbAsyncEnumerable<Movie>(fakeMovies);
            var fakeUsersAsynEnumerable = new FakeDbAsyncEnumerable<User>(users);

            var mockDbSetMovies = new Mock<IDbSet<Movie>>();
            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeAsynEnumerable.AsQueryable().Provider);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeAsynEnumerable.AsQueryable().Expression);

            mockDbSetMovies.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeAsynEnumerable.AsQueryable().ElementType);

            mockDbSetMovies.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<Movie>)fakeAsynEnumerable).GetAsyncEnumerator());

            var mockDbSetUsers = new Mock<IDbSet<User>>();
            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.Provider)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().Provider);

            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.Expression)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().Expression);

            mockDbSetUsers.As<IQueryable>()
                .Setup(mock => mock.ElementType)
                .Returns(fakeUsersAsynEnumerable.AsQueryable().ElementType);

            mockDbSetUsers.As<IDbAsyncEnumerable>()
                .Setup(mock => mock.GetAsyncEnumerator())
                .Returns(((IDbAsyncEnumerable<User>)fakeUsersAsynEnumerable).GetAsyncEnumerator());

            var dbContext = new Mock<IDbContext>();

            dbContext.Setup(x => x.Movies).Returns(mockDbSetMovies.Object);
            dbContext.Setup(x => x.Users).Returns(mockDbSetUsers.Object);

            #endregion

            Func<IDbContext> dbContextFunc = () => dbContext.Object;
            var formatter = new Mock<IAverageRatingFormatter>();
            formatter.Setup(x => x.Format(It.IsAny<double>())).Returns(2);

            // Act
            IUserRatingsService userRatingService = new UserRatingsService(dbContextFunc, formatter.Object);

            // Assert
            Assert.ThrowsAsync<MovieNotFoundException>(async () => await userRatingService.Add(1, 2, 2));
        }

    }
}
