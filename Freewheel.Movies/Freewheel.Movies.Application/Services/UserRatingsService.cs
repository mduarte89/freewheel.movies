﻿namespace Freewheel.Movies.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Freewheel.Movies.Domain.Entities;
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Data.Entity;
    using System.Linq;
    using Freewheel.Movies.Application.Exceptions;

    /// <summary>
    /// User ratings service
    /// </summary>
    public class UserRatingsService : IUserRatingsService
    {
        /// <summary>
        /// database context function
        /// </summary>
        private readonly Func<IDbContext> databaseContext;

        /// <summary>
        /// average ratting formatter
        /// </summary>
        private readonly IAverageRatingFormatter averageRatingFormatter;

        /// <summary>
        /// User ratings service constructor
        /// </summary>
        /// <param name="databaseContext"></param>
        /// <param name="averageRatingFormatter"></param>
        public UserRatingsService(Func<IDbContext> databaseContext, IAverageRatingFormatter averageRatingFormatter)
        {
            this.databaseContext = databaseContext;
            this.averageRatingFormatter = averageRatingFormatter;
        }

        /// <summary>
        /// Gets the top rated movies based on the user
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>returns a list of movies</returns>
        public async Task<IEnumerable<MovieViewModel>> GetAsync(int userId)
        {
            List<Movie> movies = new List<Movie>();

            try
            {
                using (var context = this.databaseContext())
                {
                    movies = await context.Movies
                                            .Include(x => x.Ratings)
                                            .Where(x => x.Ratings.Any(y => y.UserId == userId))
                                            .OrderByDescending(x => x.Ratings.Average(y => y.Value))
                                            .ThenBy(x => x.Title)
                                            .ToListAsync();

                    return movies.Select(x => new MovieViewModel()
                    {
                        Id = x.Id,
                        Title = x.Title,
                        YearOfRelease = x.YearOfRelease,
                        RunningTime = x.RunningTime,
                        AverageRating = x.Ratings.Any() ? this.averageRatingFormatter.Format(x.Ratings.Average(y => y.Value)) : 0
                    });
                }
            }
            catch (Exception ex)
            {
                // TODO: MD - Catch appropriate exception
                // TODO: MD - Implement log class to write the "ex" details in the log.
                // Return a empty list to hide the exception so that the error is not shown to the client calling the api.
                return new List<MovieViewModel>();
            }
        }

        /// <summary>
        /// Adds a rating to a movie based on a user. 
        /// Returns UserNotFoundException if the user does not exist
        /// Returns MovieNotFoundException if the movies does not exist
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="movieId">movie id</param>
        /// <param name="rating">rating value</param>
        public async Task Add(int userId, int movieId, int rating)
        {
            try
            {
                using (var context = this.databaseContext())
                {
                    var user = context.Users.FirstOrDefault(x => x.Id == userId);
                    if(user == null)
                    {
                        throw new UserNotFoundException(string.Format("Could not find the user with id {0}", userId), "userId");
                    }

                    var movie = context.Movies.FirstOrDefault(x => x.Id == movieId);
                    if(movie == null)
                    {
                        throw new MovieNotFoundException(string.Format("Could not find the movie with id {0}", movieId), "movieId");
                    }

                    var currentRating = user.MovieRatings.FirstOrDefault(x => x.MovieId == movieId);
                    if(currentRating == null)
                    {
                        currentRating = new Rating()
                        {
                            MovieId = movieId,
                            Value = rating,
                            UserId = user.Id
                        };

                        user.MovieRatings.Add(currentRating);
                    }
                    else
                    {
                        currentRating.Value = rating;
                    }

                    await context.SaveChangesAsync();
                }
            }
            catch (UserNotFoundException ex)
            {
                throw;
            }
            catch (MovieNotFoundException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                // TODO: MD - Catch appropriate exception
                // TODO: MD - Implement log class to write the "ex" details in the log.
                // Return a empty list to hide the exception so that the error is not shown to the client calling the api.
            }
        }
    }
}
