﻿namespace Freewheel.Movies.Application.Services
{
    using System.Threading.Tasks;
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Linq;
    using System.Data.Entity;
    using System.Collections.Generic;
    using Freewheel.Movies.Domain.Entities;
    using System;

    /// <summary>
    /// Movie service
    /// </summary>
    public class MovieService : IMovieService
    {
        /// <summary>
        /// Database context function
        /// </summary>
        private readonly Func<IDbContext> databaseContext;

        /// <summary>
        /// average rating formatter instance
        /// </summary>
        private readonly IAverageRatingFormatter averageRatingFormatter;

        /// <summary>
        /// Movie service constructor
        /// </summary>
        /// <param name="databaseContext">database context</param>
        /// <param name="averageRatingFormatter">average rating formatter</param>
        public MovieService(Func<IDbContext> databaseContext, IAverageRatingFormatter averageRatingFormatter)
        {
            this.databaseContext = databaseContext;
            this.averageRatingFormatter = averageRatingFormatter;
        }

        /// <summary>
        /// Gets all of the movies based on the filters
        /// </summary>
        /// <param name="movieSearchDetails">movie search filter</param>
        /// <returns>Returns a list of movies</returns>
        public async Task<IEnumerable<MovieViewModel>> GetAsync(MovieSearchDetailsViewModel movieSearchDetails)
        {
            List<Movie> movies = new List<Movie>();

            try
            {
                if (movieSearchDetails.Genres != null && movieSearchDetails.Genres.Any())
                {
                    using (var context = this.databaseContext())
                    {
                        movies = await (from movie in context.Movies.Include(x => x.Genres).Include(x => x.Ratings)
                                        where (
                                                   (movieSearchDetails.Title == null || movie.Title.ToLower().Contains(movieSearchDetails.Title.ToLower())) &&
                                                   (movieSearchDetails.YearOfRelease == null || movie.YearOfRelease == movieSearchDetails.YearOfRelease) &&
                                                   (movie.Genres.Any(x => movieSearchDetails.Genres.Contains(x.Name)))
                                              )
                                        select movie).ToListAsync();
                    }

                }
                else
                {
                    using (var context = this.databaseContext())
                    {
                        movies = await (from movie in context.Movies.Include(x => x.Genres).Include(x => x.Ratings)
                                        where (
                                                   (movieSearchDetails.Title == null || movie.Title.ToLower().Contains(movieSearchDetails.Title.ToLower())) &&
                                                   (movieSearchDetails.YearOfRelease == null || movie.YearOfRelease == movieSearchDetails.YearOfRelease)
                                              )
                                        select movie).ToListAsync();
                    }
                }

                return movies.Select(x => new MovieViewModel()
                {
                    Id = x.Id,
                    Title = x.Title,
                    YearOfRelease = x.YearOfRelease,
                    RunningTime = x.RunningTime,
                    AverageRating =  x.Ratings.Any() ? this.averageRatingFormatter.Format(x.Ratings.Average(y => y.Value)) : 0
                });

            }
            catch (Exception ex)
            {
                // TODO: MD - Catch appropriate exception
                // TODO: MD - Implement log class to write the "ex" details in the log.
                // Return a empty list to hide the exception so that the error is not shown to the client calling the api.
                return new List<MovieViewModel>();
            }
        }

        /// <summary>
        /// Gets top numberOfMovies rated movies
        /// </summary>
        /// <param name="movieSearchDetails">number of movies</param>
        /// <returns>Returns a list of movies</returns>
        public async Task<IEnumerable<MovieViewModel>> GetTopRatedMoviesAsync(int numberOfMovies)
        {
            using (var context = this.databaseContext())
            {
                List<Movie> movies = new List<Movie>();

                try
                {
                    movies = await context.Movies.Include(x=>x.Ratings)
                                .OrderByDescending(x => x.Ratings.Average(y => y.Value))
                                .ThenBy(x => x.Title)
                                .Take(numberOfMovies)
                                .ToListAsync();

                    return movies.Select(x => new MovieViewModel()
                    {
                        Id = x.Id,
                        Title = x.Title,
                        YearOfRelease = x.YearOfRelease,
                        RunningTime = x.RunningTime,
                        AverageRating = x.Ratings.Any() ? this.averageRatingFormatter.Format(x.Ratings.Average(y => y.Value)) : 0
                    });

                }
                catch(Exception ex)
                {
                    // TODO: MD - Catch appropriate exception
                    // TODO: MD - Implement log class to write the "ex" details in the log.
                    // Return a empty list to hide the exception so that the error is not shown to the client calling the api.
                    return new List<MovieViewModel>();
                }
            }
        }
    }
}
