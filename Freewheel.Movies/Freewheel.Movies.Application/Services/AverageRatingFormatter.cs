﻿namespace Freewheel.Movies.Application.Services
{
    using Freewheel.Movies.Domain.Interfaces;
    using System;

    /// <summary>
    /// Average rating formatter
    /// </summary>
    public class AverageRatingFormatter : IAverageRatingFormatter
    {
        /// <summary>
        /// Formats the average, closest 0.5
        /// </summary>
        /// <param name="average"></param>
        /// <returns>formatted average</returns>
        public double Format(double average)
        {
            return Math.Round(2 * average, MidpointRounding.AwayFromZero) / 2;
        }
    }
}
