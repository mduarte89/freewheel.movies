﻿namespace Freewheel.Movies.Application.Exceptions
{
    using System;

    /// <summary>
    /// User not found Exception
    /// </summary>
    public class UserNotFoundException : ArgumentException
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">error message</param>
        public UserNotFoundException(string message) : base(message)
        {
            //TODO MD: Log error here
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="paramName">parameter name</param>
        public UserNotFoundException(string message, string paramName) : base(message, paramName)
        {
            //TODO MD: Log error here
        }
    }
}
