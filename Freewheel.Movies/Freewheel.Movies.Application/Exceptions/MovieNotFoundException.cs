﻿namespace Freewheel.Movies.Application.Exceptions
{
    using System;

    /// <summary>
    /// Movie not found Exception
    /// </summary>
    public class MovieNotFoundException : ArgumentException
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">error message</param>
        public MovieNotFoundException(string message) : base(message)
        {
            //TODO MD: Log error here
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="paramName">parameter name</param>
        public MovieNotFoundException(string message, string paramName) : base(message, paramName)
        {
            //TODO MD: Log error here
        }
    }
}
