﻿using Freewheel.Movies.Data.Mappings;
using Freewheel.Movies.Domain.Entities;
using Freewheel.Movies.Domain.Interfaces;
using System.Data.Entity;

namespace Freewheel.Movies.Data
{
    public class MoviesContext : DbContext, IDbContext
    {
        public MoviesContext() : base("MoviesContext")
        {
        }

        public virtual IDbSet<Movie> Movies { get; set; }
        public virtual IDbSet<Genre> Genres { get; set; }
        public virtual IDbSet<Rating> MovieRatings { get; set; }
        public virtual IDbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MovieMap());
            modelBuilder.Configurations.Add(new GenreMap());
            modelBuilder.Configurations.Add(new RatingMap());
            modelBuilder.Configurations.Add(new UserMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
