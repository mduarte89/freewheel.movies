namespace Freewheel.Movies.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsers : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MovieRatings");
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Movies", "RunningTime", c => c.Int(nullable: false));
            AddColumn("dbo.MovieRatings", "UserId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.MovieRatings", new[] { "MovieId", "UserId" });
            CreateIndex("dbo.MovieRatings", "UserId");
            AddForeignKey("dbo.MovieRatings", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.MovieRatings", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MovieRatings", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.MovieRatings", "UserId", "dbo.Users");
            DropIndex("dbo.MovieRatings", new[] { "UserId" });
            DropPrimaryKey("dbo.MovieRatings");
            DropColumn("dbo.MovieRatings", "UserId");
            DropColumn("dbo.Movies", "RunningTime");
            DropTable("dbo.Users");
            AddPrimaryKey("dbo.MovieRatings", new[] { "Id", "MovieId" });
        }
    }
}
