namespace Freewheel.Movies.Data.Migrations
{
    using Freewheel.Movies.Domain.Entities;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<MoviesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MoviesContext context)
        {
            //if (!System.Diagnostics.Debugger.IsAttached)
            //    System.Diagnostics.Debugger.Launch();

            var comedy = new Genre()
            {
                Id = 1,
                Name = "Comedy"
            };

            var action = new Genre()
            {
                Id = 2,
                Name = "Action"
            };

            var drama = new Genre()
            {
                Id = 3,
                Name = "Drama"
            };

            context.Genres.AddOrUpdate(x => x.Id, comedy);

            context.Genres.AddOrUpdate(x => x.Id, action);

            context.Genres.AddOrUpdate(x => x.Id, drama);

            User userOne = new User
            {
                Id = 1
            };

            User userTwo = new User
            {
                Id = 2
            };

            User userThree = new User
            {
                Id = 2
            };

            context.Users.AddOrUpdate(x => x.Id, userOne);
            context.Users.AddOrUpdate(x => x.Id, userTwo);
            context.Users.AddOrUpdate(x => x.Id, userThree);


            Rating inceptionRating = new Rating()
            {
                Value = 5,
                User = userTwo
            };

            Rating madMaxRating = new Rating()
            {
                Value = 4,
                User = userOne
            };

            Rating madMaxRatingThree = new Rating()
            {
                Value = 5,
                User = userThree
            };

            Rating madMaxRatingFour = new Rating()
            {
                Value = 6,
                User = userTwo
            };

            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 1,
                Title = "Inception",
                RunningTime = 90,
                YearOfRelease = 2000,
                Genres = new List<Genre>() { drama },
                Ratings = new List<Rating>() { inceptionRating }
            });

            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 2,
                Title = "Mad Max",
                RunningTime = 180,
                YearOfRelease = 1989,
                Genres = new List<Genre>() { drama, action },
                Ratings = new List<Rating>() { madMaxRating, madMaxRatingThree , madMaxRatingFour }
            });


            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 3,
                Title = "Dumb and Dumber",
                RunningTime = 180,
                YearOfRelease = 2005,
                Genres = new List<Genre>() { comedy },
            });

            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 4,
                Title = "Braveheart",
                RunningTime = 180,
                YearOfRelease = 2000,
                Genres = new List<Genre>() { action },
            });

            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 5,
                Title = "Departed",
                RunningTime = 180,
                YearOfRelease = 2000,
                Genres = new List<Genre>() { action },
            });


            context.Movies.AddOrUpdate(x => x.Id, new Movie()
            {
                Id = 6,
                Title = "Catch me if you can",
                RunningTime = 180,
                YearOfRelease = 2003,
                Genres = new List<Genre>() { action },
            });

            context.SaveChanges();
        }
    }
}
