﻿using Freewheel.Movies.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freewheel.Movies.Data.Mappings
{
    public class MovieMap : EntityTypeConfiguration<Movie>
    {
        public MovieMap()
        {
            HasKey(t => new { t.Id });

            // Properties
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            ToTable("Movies");

            // Relationships
            this.HasMany(t => t.Genres)
                .WithMany(t => t.Movies)
                .Map(m =>
                {
                    m.ToTable("MovieGenres");
                    m.MapLeftKey("MovieId");
                    m.MapRightKey("GenreId");
                });
        }
    }
}
