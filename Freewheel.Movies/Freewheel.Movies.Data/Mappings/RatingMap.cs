﻿using Freewheel.Movies.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freewheel.Movies.Data.Mappings
{
    public class RatingMap : EntityTypeConfiguration<Rating>
    {
        public RatingMap()
        {
            HasKey(t => new { t.MovieId, t.UserId });

            ToTable("MovieRatings");

            // Relationships
            this.HasRequired(t => t.Movie)
                .WithMany(t => t.Ratings)
                .HasForeignKey(d => d.MovieId);

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.MovieRatings)
                .HasForeignKey(d => d.UserId);
        }
    }
}
