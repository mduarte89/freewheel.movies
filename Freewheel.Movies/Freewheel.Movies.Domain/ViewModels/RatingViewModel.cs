﻿namespace Freewheel.Movies.Domain.ViewModels
{
    using Newtonsoft.Json;

    /// <summary>
    /// Rating view model
    /// </summary>
    public class RatingViewModel
    {
        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
