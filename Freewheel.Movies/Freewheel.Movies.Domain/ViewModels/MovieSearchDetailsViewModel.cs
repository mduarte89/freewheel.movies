﻿namespace Freewheel.Movies.Domain.ViewModels
{
    using System.Collections.Generic;

    /// <summary>
    /// Movie search details view model
    /// </summary>
    public class MovieSearchDetailsViewModel
    {
        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the year of release
        /// </summary>
        public int? YearOfRelease { get; set; }

        /// <summary>
        /// Gets or sets the genres
        /// </summary>
        public List<string> Genres { get; set; }
    }
}
