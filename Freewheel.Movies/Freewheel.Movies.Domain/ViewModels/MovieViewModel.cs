﻿namespace Freewheel.Movies.Domain.ViewModels
{
    using Newtonsoft.Json;

    /// <summary>
    /// Movie view model
    /// </summary>
    public class MovieViewModel
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the year of release
        /// </summary>
        [JsonProperty("yearOfRelease")]
        public int YearOfRelease { get; set; }

        /// <summary>
        /// Gets or sets the running time
        /// </summary>
        [JsonProperty("runningTime")]
        public int RunningTime { get; set; }

        /// <summary>
        /// Gets or sets the average rating
        /// </summary>
        [JsonProperty("averateRating")]
        public double AverageRating { get; set; }
    }
}
