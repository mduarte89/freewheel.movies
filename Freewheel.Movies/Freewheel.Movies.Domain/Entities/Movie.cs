﻿namespace Freewheel.Movies.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// Movie entity
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the year of release
        /// </summary>
        public int YearOfRelease { get; set; }

        /// <summary>
        /// Gets or sets the running time
        /// </summary>
        public int RunningTime { get; set; }

        /// <summary>
        /// Gets or sets the genres
        /// </summary>
        public virtual ICollection<Genre> Genres { get; set; }

        /// <summary>
        /// Gets or sets the ratings
        /// </summary>
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
