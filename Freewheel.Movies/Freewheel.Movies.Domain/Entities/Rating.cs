﻿namespace Freewheel.Movies.Domain.Entities
{
    /// <summary>
    /// Ratings entity
    /// </summary>
    public class Rating
    {
        /// <summary>
        /// Gets or sets the value
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets the movie id
        /// </summary>
        public int MovieId { get; set; }

        /// <summary>
        /// Gets or sets the movie
        /// </summary>
        public Movie Movie { get; set; }

        /// <summary>
        /// Gets or sets the user id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public User User { get; set; }
    }
}
