﻿namespace Freewheel.Movies.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// User entity
    /// </summary>
    public class User
    {
        /// <summary>
        /// gets or sets the id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// gets or sets the movie ratings
        /// </summary>
        public virtual ICollection<Rating> MovieRatings { get; set; }
    }
}
