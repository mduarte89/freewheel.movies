﻿namespace Freewheel.Movies.Domain.Interfaces
{
    /// <summary>
    /// Average rating formatter interface
    /// </summary>
    public interface IAverageRatingFormatter
    {
        /// <summary>
        /// Formats the average, closest 0.5
        /// </summary>
        /// <param name="average"></param>
        /// <returns>formatted average</returns>
        double Format(double average);
    }
}
