﻿namespace Freewheel.Movies.Domain.Interfaces
{
    using Freewheel.Movies.Domain.ViewModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// User ratings interface
    /// </summary>
    public interface IUserRatingsService
    {
        /// <summary>
        /// Gets the top rated movies based on the user
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>returns a list of movies</returns>
        Task<IEnumerable<MovieViewModel>> GetAsync(int userId);

        /// <summary>
        /// Adds a rating to a movie based on a user. 
        /// Returns UserNotFoundException if the user does not exist
        /// Returns MovieNotFoundException if the movies does not exist
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="movieId">movie id</param>
        /// <param name="rating">rating value</param>
        Task Add(int userId, int movieId, int rating);
    }
}
