﻿namespace Freewheel.Movies.Domain.Interfaces
{
    using Freewheel.Movies.Domain.Entities;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Movie service interface
    /// </summary>
    public interface IMovieService
    {
        /// <summary>
        /// Gets all of the movies based on the filters
        /// </summary>
        /// <param name="movieSearchDetails">movie search filter</param>
        /// <returns>Returns a list of movies</returns>
        Task<IEnumerable<MovieViewModel>> GetAsync(MovieSearchDetailsViewModel movieSearchDetails);

        /// <summary>
        /// Gets top numberOfMovies rated movies
        /// </summary>
        /// <param name="movieSearchDetails">number of movies</param>
        /// <returns>Returns a list of movies</returns>
        Task<IEnumerable<MovieViewModel>> GetTopRatedMoviesAsync(int numberOfMovies);
    }
}
