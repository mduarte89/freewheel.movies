﻿using Freewheel.Movies.Domain.Entities;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Freewheel.Movies.Domain.Interfaces
{
    public interface IDbContext : IDisposable
    {
        IDbSet<Movie> Movies { get; set; }
        IDbSet<Genre> Genres { get; set; }
        IDbSet<Rating> MovieRatings { get; set; }
        IDbSet<User> Users { get; set; }
        Task<int> SaveChangesAsync();
    }
}
