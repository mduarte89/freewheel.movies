﻿namespace Freewheel.Movies.API.App_Start
{
    using Autofac;
    using Autofac.Integration.WebApi;
    using Freewheel.Movies.Application.Services;
    using Freewheel.Movies.Data;
    using Freewheel.Movies.Domain.Interfaces;
    using System.Reflection;
    using System.Web.Http;

    /// <summary>
    /// Container manager
    /// </summary>
    public class ContainerManager
    {
        /// <summary>
        /// Registers the instances on the container
        /// </summary>
        public static void Register()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<MoviesContext>().As<IDbContext>().InstancePerRequest();
            builder.RegisterType<MovieService>().As<IMovieService>().InstancePerRequest();
            builder.RegisterType<UserRatingsService>().As<IUserRatingsService>().InstancePerRequest();
            builder.RegisterType<AverageRatingFormatter>().As<IAverageRatingFormatter>().InstancePerRequest();


            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}