﻿namespace Freewheel.Movies.API.Controllers
{
    using Freewheel.Movies.Application.Exceptions;
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    /// <summary>
    /// User ratings controller
    /// </summary>
    [RoutePrefix("api/v1/users")]
    public class UserRatingsController : ApiController
    {
        public UserRatingsController(IUserRatingsService userRatingsService)
        {
            this.userRatingsService = userRatingsService;
        }

        /// <summary>
        /// user rating service
        /// </summary>
        private readonly IUserRatingsService userRatingsService;

        /// <summary>
        /// Gets a list of movies that the user has ratings on
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>returns a list of movies</returns>
        [Route("{userId}/top-ratings")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAsync(int userId)
        {
            if(userId == 0)
            {
                return BadRequest();
            }

            var movies = await this.userRatingsService.GetAsync(userId);

            if (!movies.Any())
            {
                return NotFound();
            }

            return Ok(movies);
        }

        /// <summary>
        /// Adds a new rating to a movie
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="movieId">movie id</param>
        /// <param name="rating">rating value</param>
        [Route("{userId}/movie/{movieId}/rating/")]
        [HttpPost]
        public async Task<IHttpActionResult> PostAsync(int userId, int movieId, RatingViewModel rating)
        {
            if (rating == null || rating.Value <= 0 || rating.Value > 5)
            {
                return BadRequest();
            }

            try
            {
                await this.userRatingsService.Add(userId, movieId, rating.Value);
            }
            catch (UserNotFoundException ex)
            {
                return NotFound();

            }catch(MovieNotFoundException ex)
            {
                return NotFound();
            }

            return Ok();
        }

    }
}
