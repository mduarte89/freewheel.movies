﻿namespace Freewheel.Movies.API.Controllers
{
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Linq;

    /// <summary>
    /// Movies controller
    /// </summary>
    [RoutePrefix("api/v1/movies")]
    public class MoviesController : ApiController
    {
        /// <summary>
        /// Movies controller
        /// </summary>
        /// <param name="movieService"></param>
        public MoviesController(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        /// <summary>
        /// Movie service instance
        /// </summary>
        private readonly IMovieService movieService;

        /// <summary>
        /// Gets the movies based on any specified filter
        /// </summary>
        /// <param name="movie">movie filter</param>
        /// <returns>Returns a list of movies</returns>
        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAsync([FromUri] MovieSearchDetailsViewModel movie)
        {
            if(movie == null || (movie.Genres == null && string.IsNullOrEmpty(movie.Title) && movie.YearOfRelease == null))
            {
                return BadRequest();
            }

            IEnumerable<MovieViewModel> movies = await movieService.GetAsync(movie);

            if (!movies.Any())
            {
                return NotFound();
            }

            return Ok(movies);
        }
    }
}
