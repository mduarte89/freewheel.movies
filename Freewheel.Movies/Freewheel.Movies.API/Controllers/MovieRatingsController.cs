﻿namespace Freewheel.Movies.API.Controllers
{
    using Freewheel.Movies.Domain.Interfaces;
    using Freewheel.Movies.Domain.ViewModels;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    /// <summary>
    /// Movie Ratings controller
    /// </summary>
    [RoutePrefix("api/v1/movies/ratings")]
    public class MovieRatingsController : ApiController
    {
        /// <summary>
        /// Movie Ratings Controller
        /// </summary>
        /// <param name="movieService">movie service</param>
        public MovieRatingsController(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        /// <summary>
        /// movie service instance
        /// </summary>
        private readonly IMovieService movieService;

        /// <summary>
        /// Gets top 5 movies by rating
        /// </summary>
        /// <returns>Returns a list of movies</returns>
        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAsync()
        {
            return await this.GetTopRatings(5);
        }

        /// <summary>
        /// Gets top numberOfMovies movies by rating
        /// </summary>
        /// <returns>Returns a list of movies</returns>
        [Route("{numberOfMovies}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAsync(int numberOfMovies)
        {
            return await this.GetTopRatings(numberOfMovies);
        }

        /// <summary>
        /// Auxiliar method to return list of movies by rating
        /// </summary>
        /// <param name="numberOfMovies">number of movies to return</param>
        /// <returns>Returns a list of movies</returns>
        private async Task<IHttpActionResult> GetTopRatings(int numberOfMovies)
        {
            IEnumerable<MovieViewModel> movies = await movieService.GetTopRatedMoviesAsync(numberOfMovies);

            if (!movies.Any())
            {
                return NotFound();
            }

            return Ok(movies);
        }
    }
}
