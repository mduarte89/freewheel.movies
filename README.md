Solution Architecture:
The solution is based on the Onion architecture, where the domain is at the center of the application. Repositories are built on top of the domain, services are built on top of the repositories, and the API built on top of the services. This allows all parts of the solution to be decoupled and changes/united tested at any time.

.Net Framework used:
v4.7.1

Relevant Nuget packages:
DIC: Autofac
Unit tests: NUnit
Mocking framework: Moq
ORM: EF code first

Installation instuctions:
- The solution is using EF code first. Please make sure that the connection string (name = MoviesContext) is pointing to a valid server and the application has the appropriate permissions to access the database. When the application runs it will create the database, although the command update-database should be run first so data is populated through the seed method. 

Services endpoints:
http://localhost:50784/api/v1/movies - returns movies based on a filter - title, yearOfRelease and genres.
http://localhost:50784/api/v1/movies/ratings - Gets top 5 movies based on ratings
http://localhost:50784/api/v1/movies/ratings/{numberOfMovies} - Gets top 1 (could be any int) moves based on ratings
http://localhost:50784/api/v1/users{userId}/top-ratings - Gets top users top rated moved
http://localhost:50784/api/v1/{userId}/movie/{movieId}/rating/ - Add a rating to a movie for a certain user.

Points to improve (were not delivered because of the lack of time):
- There are are few places where exceptions are being handled using the type Exception, should have choosen a more specific type to handle the exception.
- Add a log solution to write logs, either to a file by writting directly to it or using Log4Net.
- Add more unit tests to cover more scenarios. Because of time constraints was just able to provide a few, but i think it would be possible to achieve high code coverage due to the solution being decoupled.
- Choose bitbucket instead of Github because its where i keep all of my personal projects.
 